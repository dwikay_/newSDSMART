#!/bin/sh

cd /var/www/html/sdsmarts-4
PORT=8082
HOST=sdsmarts-web.idx.co.id

#php artisan key:generate
/opt/php-7.2/bin/php artisan config:cache
/opt/php-7.2/bin/php artisan config:clear 
/opt/php-7.2/bin/php artisan view:clear 
/opt/php-7.2/bin/php artisan clear-compiled 
#composer dump-autoload
#/opt/php-7.2/bin/php artisan serve --host=$HOST --port=$PORT > /dev/null &
/opt/php-7.2/bin/php artisan serve --host=$HOST --port=$PORT 

#echo $! > /var/www/html/sdsmarts-4/sdsmarts-web.pid

