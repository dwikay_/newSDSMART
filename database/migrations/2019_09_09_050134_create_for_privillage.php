<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForPrivillage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('roles', function (Blueprint $table) {
        $table->increments('id');
        $table->string('role_name',100);
        $table->string('description',255);
        $table->string('akses',20);
        $table->timestamps();
      });

      Schema::create('role_acl', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('role_id')->nullable();
        $table->unsignedInteger('module_id')->nullable();
        $table->string('create_acl')->nullable();
        $table->integer('read_acl')->nullable();
        $table->integer('update_acl')->nullable();
        $table->integer('delete_acl')->nullable();
        $table->integer('module_parent');
        $table->timestamps();
      });

      Schema::create('modules', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('menu_parent')->nullable();
        $table->string('module_name');
        $table->string('menu_mask');
        $table->string('menu_path');
        $table->string('menu_icon');
        $table->integer('menu_order');
        $table->integer('divider');
        $table->timestamps();
      });

      Schema::table('users', function (Blueprint $table) {
          $table->unsignedInteger('role_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
