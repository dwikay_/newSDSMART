<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailLogUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('activityDetail', function (Blueprint $table) {
        $table->increments('id');
        $table->string('username')->nullable();
        $table->string('group')->nullable();
        $table->text('activity')->nullable();
        $table->text('activityDescription')->nullable();
        $table->string('date',20)->nullable();
        $table->string('time',20)->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
