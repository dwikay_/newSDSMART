#!/bin/sh

cd /var/www/html/sdsmarts-4

FILE=/var/www/html/sdsmarts-4/sdsmarts-web.pid
PORT=8082

if [ -f "$FILE" ]; then
	/usr/bin/ps auxww | /usr/bin/grep $PORT | /usr/bin/grep -v "grep " | /usr/bin/awk '{print $2}' | /usr/bin/xargs kill -9
	/usr/bin/rm -rf $FILE
fi



