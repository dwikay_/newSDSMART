<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\Model\Roleacl;
use Auth;
use Illuminate\Http\Request;
use Session;
use App\Library\activityLog;
// use Adldap\AdldapInterface;
use Adldap;
// use Adldap\Laravel\Facades\Adldap;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/monitoring/monJobs';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getLogin(){

      if(Session::get('users')!=null){
        return redirect(url('/monitoring/monJobs'));
      }

      return view('auth.login');
    }

    public function logout(activityLog $activityLog){

      $act = "Logout";
      $desc = "Logout Aplikasi";
      $activityLog->logUser($act, $desc);

      // Session::getHandler()->destroy(Session::get('users'));
      Session::forget('users');

      return redirect(url('login'));
    }

    public function postLoginLdap(Request $request, activityLog $activityLog){

      $users = Adldap::search()->where('uid',$request->email)->first();

        $cek_user = User::where('email', $request->email)->first();
        // dd($cek_user);
        if($cek_user!=null){

          if($cek_user->verify_password=="0" || $cek_user->verify_password==0){

            return redirect(url('verification/'.$username));
          }
          else if($cek_user->limit_password==3 || $cek_user->limit_password=="3"){

            // $alert = "";

            Session::flash('info', 'Error');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-times');
            Session::flash('alert', 'Your account has been banned. please contact to superadmin!');
            return redirect(url('login'));

          }
          else{

            if(Adldap::auth()->attempt($users->distinguishedname[0],$request->password)){

              if($cek_user->limit_password==3 || $cek_user->limit_password=="3"){

                $alert = "Your account has been banned. please contact to superadmin!";
                Session::forget('users');

              } else {

                $cek_role = Roleacl::where('role_id', $cek_user->role_id)
                ->where('create_acl', null)
                ->where('read_acl', null)
                ->where('update_acl', null)
                ->where('delete_acl', null)
                ->get();

                if(count($cek_role)==0){

                  $cek_user = User::where('email', $request->email)->first();
                  Session::put('email',$request->email);
                  Session::put('role_id',$cek_user->role_id);
                  Session::put('users', $cek_user);
                  $act = "Login";
                  $desc = "Login Aplikasi";
                  $activityLog->logUser($act, $desc);
                  // return count($cek_role);
                  return redirect(url('/monitoring/monJobs'));

                } else if(count($cek_role)<0){
                  // return count($cek_role);
                  $cek_user = User::where('email', $request->email)->first();
                  Session::put('email',$request->email);
                  Session::put('role_id',$cek_user->role_id);
                  Session::put('users', $cek_user);
                  $act = "Login";
                  $desc = "Login Aplikasi";
                  $activityLog->logUser($act, $desc);

                  return redirect(url('/monitoring/monJobs'));
                }else{

                  Session::forget('users');
                  Session::flash('info', 'Error');
                  Session::flash('colors', 'red');
                  Session::flash('icons', 'fas fa-times');
                  Session::flash('alert', 'You dont have any access, please contact your superadmin!');
                  return redirect(url('login'));

                }

              }


            }else{
              if($cek_user!=null){
                $cek_users = User::where('email', $request->email)->first();
                $cek_users->limit_password = $cek_user->limit_password+1;
                $cek_users->save();

                Session::flash('info', 'Error');
                Session::flash('colors', 'red');
                Session::flash('icons', 'fas fa-times');
                Session::flash('alert', 'Please check username or password');
                return redirect(url('login'));

              }
            }

          }


        }else{
          Session::flash('info', 'Error');
          Session::flash('colors', 'red');
          Session::flash('icons', 'fas fa-times');
          Session::flash('alert', 'Please check username or password');
          return redirect(url('login'));
        }

      }


    public function postLogin(Request $r, activityLog $activityLog){
        $username = $r->input('email');
        $password = $r->input('password');
        $remember = ($r->input('remember')) ? true : false;

        $cek_user = User::where('email', $username)->first();
        // return $cek_user;
        if($cek_user!=null){
          if($cek_user->verify_password=="0" || $cek_user->verify_password==0){

            return redirect(url('verification/'.$username));
          }
          else if($cek_user->limit_password==3 || $cek_user->limit_password=="3"){

            $alert = "Your account has been banned. please contact to superadmin!";

          }

          else{

          if (Auth::attempt(['email' => $username, 'password' => $password])) {
            if($cek_user->limit_password==3 || $cek_user->limit_password=="3"){

              $alert = "Your account has been banned. please contact to superadmin!";
              Auth::logout();

            }else{

              $cek_role = Roleacl::where('role_id', $cek_user->role_id)
              ->where('create_acl', null)
              ->where('read_acl', null)
              ->where('update_acl', null)
              ->where('delete_acl', null)
              ->get();

              if(count($cek_role)==0){
                $act = "Login";
                $desc = "Login Aplikasi";
                $activityLog->logUser($act, $desc);

                return redirect(url('/monitoring/monJobs'));

              } else if(count($cek_role)>0){
                $act = "Login";
                $desc = "Login Aplikasi";
                $activityLog->logUser($act, $desc);

                return redirect(url('/monitoring/monJobs'));
              }else{

                $alert = "You dont have any access, please contact your superadmin!";
                Auth::logout();

              }

            }

          }else{
            if($cek_user!=null){
              $cek_users = User::where('email', $username)->first();
              $cek_users->limit_password = $cek_user->limit_password+1;
              $cek_users->save();
              $alert = "Please Check Username / Password";
            }

          }
        }
      }else{
        $alert = "Please Check Username / Password";
      }
          // dd("ini");
          Session::flash('info', 'Error');
          Session::flash('colors', 'red');
          Session::flash('icons', 'fas fa-times');
          Session::flash('alert', $alert);
          return redirect()->back();

      }
}
