<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use Session;
use App\Library\activityLog;

class ConfigController extends Controller
{

    public function Checkdelete($id, $typeDB){

      if($typeDB==0){
        $cekTask = AdmTask::where('DBSOURCEID', $id)->count();
      }else{
        $cekTask = AdmTask::where('DBDESTID', $id)->count();
      }


      if($cekTask==0){
        $notif = "kosong";
        // $cek_taskz = AdmTask::where('')
      }else{
        $notif = "already";
      }

      $data[] = array(
        "notif" => $notif
      );

      return json_encode($data);
    }
    public function indexConfig(activityLog $activityLog){

      $act = "DB Config";
      $desc = "Opening DB Config";
      $activityLog->logUser($act, $desc);

      return view('management.dbconfig.index');
    }

    public function testConnection(Request $request, CurlGenerator $curlGen, activityLog $activityLog){

      $provider = $request->providerName;
      $ip = $request->ipAddress;
      $port = $request->port;
      $db = $request->databaseName;
      $username = $request->userID;
      $password = $request->password;
      $urls = "/datasnap/rest/TRESTMethods/TestConnection"."/".$provider.";".$ip.";".$port.";".$db.";".$username.";".$password.";";
      // return $urls;
      $param = $curlGen->testConnection($urls);

      $act = "Test Connection";
      $desc = "Testing Connection to service with result ".$param;
      $activityLog->logUser($act, $desc);

      return json_encode($param);

    }

    public function delete($id, activityLog $activityLog){

      $AdmDBCon = AdmDBCon::where('DBCONNID', $id)->first();
      // return $AdmDBCon;
      $act = "DB Config";
      $desc = "Deleted Config Where Value ".$AdmDBCon;
      $activityLog->logUser($act, $desc);

      AdmDBCon::where('DBCONNID', $id)->delete();

      return redirect(url('management/config'));
    }
    public function getIndex(){

      $user = AdmDBCon::all();


      return Datatables::of($user)->escapeColumns([])->make(true);

    }
    public function createConfig(activityLog $activityLog){

      $act = "DB Config";
      $desc = "Opening Form Create Config";
      $activityLog->logUser($act, $desc);

      return view('management.dbconfig.config');
    }
    public function store(Request $request, activityLog $activityLog){

      date_default_timezone_set('Asia/Jakarta');

      // return $request->all();
      $dbcon = new AdmDBCon();
      $dbcon->DBSERVERNAME = $request->systemName;
      $dbcon->DBPROVIDER = $request->providerName;
      $dbcon->DBHOSTNAME = $request->ipAddress;
      $dbcon->DBUSERID = $request->userID;
      $dbcon->DBPASSWORD = $request->password;
      $dbcon->DBPORT = $request->port;
      $dbcon->DBNAME = $request->databaseName;
      $dbcon->TYPEDB = $request->jenisDB;
      $dbcon->INSUI = Session::get('users')->name;
      $dbcon->INSDA = date('Y-m-d H:i:s');
      $dbcon->save();


      $admcon = AdmDBCon::orderby('DBCONNID','desc')->first();
      $act = "DB Config";
      $desc = "Saving Config With Value ".$admcon;
      $activityLog->logUser($act, $desc);

      return redirect(url('management/config'));

    }
    public function edit($id, activityLog $activityLog){

      try {

        $dbcon = AdmDBCon::findOrFail($id);

        $act = "DB Config";
        $desc = "Opening Form Edit Config";
        $activityLog->logUser($act, $desc);

        return view('management.dbconfig.edit')
        ->with('dbcon', $dbcon);

      } catch (ModelNotFoundException $e) {

        return redirect(url('management/config'))
        ->with('errmsg','Provider Name cant find')
        ->with('classerr','danger');
      }

    }
    public function update(Request $request, activityLog $activityLog, $id){

      try {

        $dbcon = AdmDBCon::findOrFail($id);

        $dbcon->DBSERVERNAME = $request->systemName;
        $dbcon->DBPROVIDER = $request->providerName;
        $dbcon->DBNAME = $request->databaseName;
        $dbcon->DBHOSTNAME = $request->ipAddress;
        $dbcon->DBPORT = $request->port;
        $dbcon->DBUSERID = $request->userID;
        $dbcon->DBPASSWORD = $request->password;
        $dbcon->MODUI = Session::get('users')->name;
        $dbcon->TYPEDB = $request->jenisDB;
        $dbcon->MODDA = date('Y-m-d H:i:s');
        $dbcon->save();

        $dbconNew = AdmDBCon::orderby('DBCONNID','desc')->first();

        $act = "DB Config";
        $desc = "Update Config with value ".$dbconNew;
        $activityLog->logUser($act, $desc);

        return redirect(url('management/config'))
        ->with('errmsg','Provider Name cant find')
        ->with('classerr','danger');

      } catch (ModelNotFoundException $e) {
        $dbcon = AdmDBCon::findOrFail($id);
        return view('management.dbconfig.edit')
        ->with('dbcon', $dbcon);
      }

    }
    public function indexMapping(activityLog $activityLog){

      $act = "Task Configuration";
      $desc = "Opening Form Task Configuration";
      $activityLog->logUser($act, $desc);

      return view('management.dbconfig.mapping');
    }



}
