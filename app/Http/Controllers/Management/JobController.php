<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\RecordLog;
use App\Model\AdmTask;
use App\Model\AdmRowNum;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Library\activityLog;

class JobController extends Controller
{
  public function indexJob(activityLog $activityLog){
    $task = AdmTask::all();

    $act = "Manual Run";
    $desc = "Opening Manual Run";
    $activityLog->logUser($act, $desc);

    return view('management.manualRun.index')
    ->with('task', $task);
  }

  public function checkRecord($id, $tanggal){

    $query = RecordLog::where('TASKID', $id)->where('RECORDDATE', $tanggal)->first();

    $record = $query->RECORDCOUNT;

    return json_encode($record);
  }
  public function startJob(activityLog $activityLog, CurlGenerator $curlGen, $id, $tanggal){

    $url = "/datasnap/rest/TRESTMethods/ManualJob/".$id.";".$tanggal;
    $param = $curlGen->getIndex($url);

    $act = "Monitoring Jobs";
    $desc = "Start Job Monitoring Jobs ".$id;
    $activityLog->logUser($act, $desc);

    return json_encode($param);

  }
  public function cancelJob(activityLog $activityLog, CurlGenerator $curlGen, $id){

    $url = "/datasnap/rest/TRESTMethods/stoptask/".$id;
    $param = $curlGen->getIndex($url);

    $act = "Monitoring Jobs";
    $desc = "Stop Job Monitoring Jobs ".$id;
    $activityLog->logUser($act, $desc);

    return json_encode($param);

  }
  public function initialData(activityLog $activityLog, CurlGenerator $curlGen, $id, $tanggal){

    $url = "/datasnap/rest/TRESTMethods/InitialData/".$id.";".$tanggal;
    $param = $curlGen->getIndex($url);

    // return $param;
    $explodeResult = explode(',', $param);
    // return $explodeResult[0];

    $cek_task = AdmTask::where('TASKID', $id)->first();

    if($cek_task->ISINSERT == "1" || $cek_task->ISINSERT == 1){
      if($explodeResult[0]=="00" || $explodeResult[0]==00){
        $updateRownum = AdmRowNum::where('TASKID', $id)->first();
        // return $updateRownum;
        $updateRownum->ROWNUM = 0;
        $updateRownum->save();
      }
    }


    $act = "Monitoring Jobs";
    $desc = "Initial Data on Job Monitoring Jobs ".$id;
    $activityLog->logUser($act, $desc);

    return json_encode($explodeResult[1]);

  }


}
