<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\Log;
use App\Model\Audit;
use App\Model\AdmRowNum;
use App\Model\activityUser;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use App\Library\activityLog;

class UserBckLogController extends Controller
{
  public function index(activityLog $activityLog){

    $act = "Backup User Log Activity";
    $desc = "Opening Backup User Log Activity";
    $activityLog->logUser($act, $desc);

    return view('users.backupLog.index');
  }

  public function download(Request $request, activityLog $activityLog,$startdate, $enddate){

    $log = activityUser::with('role')->whereBetween('date', [$request->startDate, $request->endDate])->get();

    $act = "Download Backup User Log Activity";
    $desc = "Download Backup User Log Activity on date ".$startdate." sd ".$enddate;
    $activityLog->logUser($act, $desc);

    Excel::create("UserLog-SDSMARTS".$startdate." ".$enddate, function ($result) use ($log) {
  				$result->sheet('SheetName', function ($sheet) use ($log) {
            $i = 1;
  						foreach ($log as $key => $item) {

  							$data=[];
  							array_push($data, array(
                $i++,
  							$item->user_type,
  							$item->users->name,
  							$item->event,
  							$item->auditable_type,
  							$item->old_values,
  							$item->new_values,
                $item->url,
                $item->ip_address,
                $item->user_agent,
                $item->tags

  					));
  								$sheet->fromArray($data, null, 'A2', false, false);
  						}
  						$sheet->row(1, array(
                'No',
                'Username',
                'Group',
                'Activity',
                'Activity Description',
                'Date',
                'Time'));

  						$sheet->setBorder('A1:K1', 'thin');
  						$sheet->cells('A1:K1', function ($cells) {
  								$cells->setBackground('#0070c0');
  								$cells->setFontColor('#ffffff');
  								$cells->setValignment('center');
  								$cells->setFontSize('11');
  						});
  						$sheet->setHeight(array(
  					'1' => '20'
  			));
  						$sheet->setWidth('A', '10');
  						$sheet->setWidth('B', '25');
  						$sheet->setWidth('C', '25');
  						$sheet->setWidth('D', '25');
  						$sheet->setWidth('E', '100');
  						$sheet->setWidth('F', '25');
  						$sheet->setWidth('G', '25');
  				});
  				ob_end_clean();
  		})->download('xls');

  }
}
