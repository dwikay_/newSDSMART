<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role;
use App\Model\Role_acl;
use App\Model\Module;
use App\User;
use Session;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use App\Library\activityLog;
use Adldap\Laravel\Facades\Adldap;

class UserManagementController extends Controller
{
    public function getLdap(){

      // $users = Adldap::getDefaultProvider()->search()->get();
      // $users = Adldap::getDefaultProvider()->search()->get();
      $search = Adldap::search()->get();
      // $search = Adldap::search()->where('cn', '=', 'nanda.novain')->get();
      // return $search;
      $ldapUsers = [];
      foreach ($search as $key => $value) {
        $ldapUsers[] = [
          'sn' => $value->sn[0],
          'uid' => $value->uid[0],
          'cn' => $value->cn[0],
          'description' => $value->description[0],
          'userpassword' => $value->userpassword[0]
        ];
      }
      return $ldapUsers;

    }
    public function disabled($ids){

      $cek_user = User::find($ids);
      $cek_user->limit_password = 3;
      $cek_user->save();

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'User Disabled');
      return redirect(url('users/management'));

    }

    public function enabled($ids){

      $cek_user = User::find($ids);
      $cek_user->limit_password = 0;
      $cek_user->save();

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'User Enabled');
      return redirect(url('users/management'));

    }
    public function index(activityLog $activityLog){

      $act = "Administrator";
      $desc = "Opening User Management";
      $activityLog->logUser($act, $desc);

      return view('users.userManagement.index');
    }

    public function getUsers(){

      $user = User::with('role')->get();

      return Datatables::of($user)->escapeColumns([])->make(true);
    }

    public function create(activityLog $activityLog){

      $role = Role::where('id','!=',1)->get();
      $ldap = $this->getLdap();

      $act = "Administrator";
      $desc = "Opening From Create User Management";
      $activityLog->logUser($act, $desc);

      return view('users.userManagement.create')
      ->with('role', $role)
      ->with('userLdap', $ldap);
    }

    public function store(Request $request, activityLog $activityLog){

      $date = date('Y-m-d');
      $dates = date('Y-m-d', strtotime($date. ' + 90 days'));

      $user = new User();
      $user->name = $request->nama;
      $user->email = $request->email;
      $user->password = bcrypt($request->nama);
      $user->role_id = $request->role;
      $user->expired_password = $dates;
      $user->verify_password = 1;
      $user->save();

      $act = "Administrator";
      $desc = "Saving User Management";
      $activityLog->logUser($act, $desc);

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Saved!');
      return redirect(url('users/management'));
    }

    public function update(Request $request,activityLog $activityLog, $id){

      $user = User::find($id);

      $user->name = $request->nama;
      $user->email = $request->email;
      if($request->password!=""){
        $user->password = bcrypt($request->password);
      }
      $user->role_id = $request->role;
      $user->save();


      $act = "Administrator";
      $desc = "Update User Management with username ".$request->email;
      $activityLog->logUser($act, $desc);

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Saved!');
      return redirect(url('users/management'));

    }

    public function edit(activityLog $activityLog, $id){

      try {

      $user = User::with('role')->where('id',$id)->firstOrFail();
      $role = Role::all();
      $ldap = $this->getLdap();

      $act = "Administrator";
      $desc = "Opening Form Editor User Management";
      $activityLog->logUser($act, $desc);

      return view('users.userManagement.edit')
      ->with('user', $user)
      ->with('role', $role)
      ->with('userLdap', $ldap);

      } catch (ModelNotFoundException $e) {
        Session::flash('info', 'Error');
        Session::flash('colors', 'red');
        Session::flash('icons', 'fas fa-times');
        Session::flash('alert', 'User not found!');
        return redirect(url('users/management'));
      }

    }

    public function delete(activityLog $activityLog, $id){

      $user = User::find($id);

      $act = "Administrator";
      $desc = "Delete User Management username ".$user->email;
      $activityLog->logUser($act, $desc);

      User::destroy($id);
      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Deleted');
      return redirect(url('users/management'));
    }

}
