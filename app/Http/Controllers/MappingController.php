<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\AdmRowNum;
use App\Model\RecordLog;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use Session;
use App\Library\activityLog;

class MappingController extends Controller
{
  public function changeFieldSource($dbconnid,$tablename, $columns,$type, DBConnection $DBConnection){
    $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();
    $column1 = explode('-', $columns);
    // return $tbls;
    $column = $column1[1];
    // return $tbl;
    $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);

    $columns = array();

    // $TABLE_SCHEMA = explode('.', $tablename)[0];
    // $TABLE_NAME = explode('.', $tablename)[1];
    $TABLE_NAME = explode('.', $tablename);

    if($type=="target"){
      if($AdmDBCon->DBPROVIDER == 'SQL Server'){
        $result = $connection->table('INFORMATION_SCHEMA.COLUMNS')->select('COLUMN_NAME','DATA_TYPE')->where('TABLE_NAME',$TABLE_NAME)->where('COLUMN_NAME',$column)->where('TABLE_SCHEMA','dbo')->orderBy('COLUMN_NAME','asc')->get();
        $i = 1;
        foreach($result as $column){
          $columns[] = array(
            'id' => $i++,
            'COLUMN_NAME'=>$column->COLUMN_NAME,
            'DATA_TYPE'=>$column->DATA_TYPE,
          );
        }
      }
    }

    else{
      if($AdmDBCon->DBPROVIDER == 'PostgreSQL'){
        $result = $connection->table('information_schema.columns')->select('column_name','udt_name')->where('table_name',$TABLE_NAME)->where('column_name',$column)->where('table_schema','public')->get();
        // return $result;
        $i = 1;
        foreach($result as $column){
          $columns[] = array(
            'id' => $i++,
            'COLUMN_NAME'=>$column->column_name,
            'DATA_TYPE'=>$column->udt_name,
          );
        }
      }
    }


    $DBConnection->disconnect($AdmDBCon->DBPROVIDER);
    // return count($columns);
    echo json_encode($columns[0]);
  }

    public function index(activityLog $activityLog){

      $act = "Task Configuration";
      $desc = "Opening Task Configuration";
      $activityLog->logUser($act, $desc);

      	return view('management.mapping.index');
    }

    public function checkTaskUpdate($taskName, $id){

      $cek_task = AdmTask::where('TASKID', $id)->first();

      if ($taskName != $cek_task->TASKNAME){
        $cek_task = AdmTask::where('TASKNAME', $taskName)->first();

        if($cek_task==null){
          $status = "kosong";
        }else{
          $status = "ada";
        }
      }else{
        $status = "kosong";
      }

      $data[] = array(
        "status" => $status
      );

      return json_encode($data);

    }
    public function checkTask($taskName){

      $cek_task = AdmTask::where('TASKNAME', $taskName)->first();

      if($cek_task==null){
        $status = "kosong";
      }else{
        $status = "ada";
      }
      $data[] = array(
        "status" => $status
      );

      return json_encode($data);

    }
    public function checkDelete(CurlGenerator $curlGen, $taskid){


      $url = "/datasnap/rest/TRESTMethods/Deletetask/".$taskid;
      $param = $curlGen->getIndex($url);
      // return $param;
      $expd = explode(',', $param);
      if($expd[0]==00 || $expd[0]=="00"){
        AdmTask::where('TASKID', $taskid)->delete();
      }
      $data[] = array(
        "code" => $expd[0],
        "result" => $expd[1]
      );

      return json_encode($data);
    }
    public function deleteTask(activityLog $activityLog, $taskid){

        AdmTask::where('TASKID', $taskid)->delete();
        AdmRowNum::where('TASKID', $taskid)->delete();
        AdmTimeStamp::where('TASKID', $taskid)->delete();
        $cek_table = AdmMapTable::where('TASKID', $taskid)->first();
        AdmMapField::where('MAPTBLID', $cek_table->MAPTBLID)->delete();
        AdmMapTable::where('TASKID', $taskid)->delete();
        RecordLog::where('TASKID', $taskid)->delete();

        $act = "Task Configuration";
        $desc = "Delete Task Configuration with Task ID ".$taskid;
        $activityLog->logUser($act, $desc);

        return redirect(url('management/mapping/table'));

    }
      public function getIndex(){

      $task = AdmTask::with('dbSource','dbTarget','mapTable')->orderby('TASKNAME','asc')->get();
      // return $task;
      $data = array(
        "listMapping" => array()
      );

      foreach ($task as $key => $values) {
        // return $values->dbSource->DBSERVERNAME;
        if($values->dbSource==null){
          $dbsourceName = "####";
          $tblsource = "####";
        }else{
          $dbsourceName = $values->dbsource->DBSERVERNAME;
          $tblsource = $values->mapTable->TBLSOURCE;
        }

        if($values->dbtarget==null){
          $dbtargetName = "####";
          $tbltarget = "####";
        }else{
          $dbtargetName = $values->dbtarget->DBSERVERNAME;
          $tbltarget = $values->mapTable->TBLDEST;
        }

        if($values->ISINSERT=="1"){
          $proses_type = "Insert";
        }
        else{
          $proses_type = "Insert & Update";
        }

        $timestamp = strtotime($values->INSDA);
        $date = date('d-M-y', $timestamp);

                $data["listMapping"][$key] = array(
                    "TASKID" => $values->TASKID,
                    "TASKNAME" => $values->TASKNAME,
                    "PROSES_TYPE" => $proses_type,
                    "TASKINTERVAL" => $values->TASKINTERVAL,
                    "DBSOURCEID" => $dbsourceName,
                    "TBLSOURCE" => $tblsource,
                    "DBDESTID" => $dbtargetName,
                    "TBLDEST" => $tbltarget,
                    "DATE" => $date,
                    "CREATED_BY" => $values->INSUI
                  );
              }

      // $data['listMapping'] = array_values($data['listMapping']);

      // return $data['listMapping'];
      return Datatables::of($data['listMapping'])->escapeColumns([])->make(true);
    }

    public function reloadTask(activityLog $activityLog,CurlGenerator $curlGen){

      $url = "/datasnap/rest/TRESTMethods/Reloadtasks";
      $param = $curlGen->getIndex($url);

      $act = "Task Configuration";
      $desc = "Reload All Task Configuration";
      $activityLog->logUser($act, $desc);

      return json_encode($param);
    }

    public function getField(CurlGenerator $curlGen) {

      $url = "/datasnap/rest/TRESTMethods/GetFieldsList/2;idxindexstats;3;idxindexstats";
      $param = $curlGen->getField($url);

      // return $param;

      $expd1 = explode('/', $param);
      foreach($expd1 as $key => $value){
        $expd2 = explode(';', $value);
          foreach($expd2 as $keys => $values){
            $expd3 = explode(',', $values);
            $data[] = array(
              'field' => $expd3[0],
              'tipeData' => $expd3[1],
              'lenghtChar' => $expd3[2],
              'numericPres' => $expd3[3]
            );
        }
      }
      return $data;
    }
    public function store(Request $request,activityLog $activityLog){

      date_default_timezone_set('asia/jakarta');

      $task = new AdmTask();

      if($request->prosesTypePost=="1"){
        $isinsert = 1;
          if($request->dataTypePost=="0"){
            $isrownum = 0;
          }else{
            $isrownum = 1;
          }
      }else{
        $isinsert = 0;
        $isrownum = 0;
      }

      // return $request->all();
      $task->TASKNAME = $request->taskNamePost;
      $task->TASKINTERVAL = $request->taskIntervalPost;
      $task->ISINSERT = $isinsert;
      $task->ISROWNUM = $isrownum;
      $task->DBSOURCEID = $request->selectSourcePost;
      $task->DBDESTID = $request->selectTargetPost;
      $task->INSUI = Session::get('users')->email;
      $task->INSDA = date('Y-m-d H:i:s');
      $task->save();

      $cek_task = AdmTask::orderby('TASKID','desc')->first();

      $expdTblSource = explode('.',$request->sourceTablePost);
      $expdTblTarget = explode('.',$request->targetTablePost);
      $expdTblStagging = explode('.',$request->staggingTablePost);

      $table = new AdmMapTable();
      $table->TASKID = $cek_task->TASKID;
      $table->TBLSOURCE = $expdTblSource[0];
      $table->TBLDEST = $expdTblTarget[0];
      $table->TBLTEMP = $expdTblStagging[0];
      $table->SCRIPTQUERY = $request->queryTextPost;
      $table->INSUI = Session::get('users')->email;
      $table->INSDA = date('Y-m-d H:i:s');
      $table->save();

      $cek_table = AdmMapTable::orderby('MAPTBLID','desc')->first();
      foreach ($request->sourceFieldPost as $key => $value) {

        $explodeSource = explode('-', $request->sourceFieldPost[$key]);
        $explodeTarget = explode('-', $request->targetFieldPost[$key]);

        $field = new AdmMapField();
        $field->MAPTBLID = $cek_table->MAPTBLID;
        $field->FIELDSOURCE = $explodeSource[1];
        $field->FIELDDEST = $explodeTarget[1];
        $field->FIELDSOURCETYPE = $request->sourceTypePost[$key];
        $field->FIELDDESTTYPE = $request->targetTypesPost[$key];
        $field->ISPRIMARYKEYSOURCE = $request->inputSourcePost[$key];
        $field->ISPRIMARYKEYDEST = $request->inputTargetPost[$key];
        $field->INSUI = Session::get('users')->email;
        $field->INSDA = date('Y-m-d H:i:s');
        $field->save();
      }

      if($request->prosesTypePost=="1"){

          if($request->dataTypePost=="0"){
            $timestamp = new AdmTimeStamp();
            $timestamp->TIMESTART = $request->timestampValPost;
            $timestamp->TASKID = $cek_task->TASKID;
            $timestamp->INSUI = Session::get('users')->email;
            $timestamp->INSDA = date('Y-m-d H:i:s');
            $timestamp->save();
          }else{
            $rownum = new AdmRowNum();
            $rownum->ROWNUM = $request->rownumValPost;
            $rownum->ROWNUMDATE = date('Y-m-d');
            $rownum->TASKID = $cek_task->TASKID;
            $rownum->INSUI = Session::get('users')->email;
            $rownum->INSDA = date('Y-m-d H:i:s');
            $rownum->save();
          }

      } else {
        // dd("sini");
        // $rownum = new AdmRowNum();
        // $rownum->ROWNUM = 0;
        // $rownum->ROWNUMDATE = date('Y-m-d');
        // $rownum->TASKID = $cek_task->TASKID;
        // $rownum->INSUI = Auth::user()->name;
        // $rownum->INSDA = date('Y-m-d H:i:s');
        // $rownum->save();


      }

      $act = "Task Configuration";
      $desc = "Save Task Configuration with value ".$cek_task;
      $activityLog->logUser($act, $desc);

      $data[] = array(
        'result' => 'success'
      );

      return json_encode($data);
    }

    public function update(Request $request,activityLog $activityLog){

      date_default_timezone_set('asia/jakarta');
      // return $request->all();

      $task = AdmTask::where('TASKID', $request->idTaskPost)->first();

      if($request->prosesTypePost=="1"){
        $isinsert = 1;
          if($request->dataTypePost=="0"){
            $isrownum = 0;
          }else{
            $isrownum = 1;
          }
      }else{
        $isinsert = 0;
        $isrownum = 0;
      }
      // return json_encode($task);
      $task->TASKNAME = $request->taskNamePost;
      $task->TASKINTERVAL = $request->taskIntervalPost;
      $task->ISINSERT = $isinsert;
      $task->ISROWNUM = $isrownum;
      $task->DBSOURCEID = $request->selectSourcePost;
      $task->DBDESTID = $request->selectTargetPost;
      $task->INSUI = Session::get('users')->email;
      $task->INSDA = date('Y-m-d H:i:s');
      $task->save();

      $cek_task = AdmTask::where('TASKID', $request->idTaskPost)->first();

      $expdTblSource = explode('.',$request->sourceTablePost);
      $expdTblTarget = explode('.',$request->targetTablePost);
      $expdTblStagging = explode('.',$request->staggingTablePost);
      // return $cek_task->TASKID;
      $table = AdmMapTable::where('MAPTBLID', $request->idTablePost)->first();
      // return $expdTblStagging[0];
      $table->TASKID = $cek_task->TASKID;
      $table->TBLSOURCE = $expdTblSource[0];
      $table->TBLDEST = $expdTblTarget[0];
      $table->TBLTEMP = $expdTblStagging[0];
      $table->SCRIPTQUERY = $request->queryTextPost;
      $table->INSUI = Session::get('users')->email;
      $table->INSDA = date('Y-m-d H:i:s');
      $table->save();

      $cek_table = AdmMapTable::where('MAPTBLID', $request->idTablePost)->first();

      $removeField = AdmMapField::where('MAPTBLID', $request->idTablePost)->delete();

      foreach ($request->sourceFieldPost as $key => $value) {

        $explodeSource = explode('-', $request->sourceFieldPost[$key]);
        $explodeTarget = explode('-', $request->targetFieldPost[$key]);

        $field = new AdmMapField();
        $field->MAPTBLID = $cek_table->MAPTBLID;
        $field->FIELDSOURCE = $explodeSource[1];
        $field->FIELDDEST = $explodeTarget[1];
        $field->FIELDSOURCETYPE = $request->sourceTypePost[$key];
        $field->FIELDDESTTYPE = $request->targetTypesPost[$key];
        $field->ISPRIMARYKEYSOURCE = $request->inputSourcePost[$key];
        $field->ISPRIMARYKEYDEST = $request->inputTargetPost[$key];
        $field->INSUI = Session::get('users')->email;
        $field->INSDA = date('Y-m-d H:i:s');
        $field->save();
      }

      if($request->prosesTypePost=="1" || $request->prosesTypePost==1){

        if($request->dataTypePost=="0"){
          AdmRowNum::where('TASKID', $request->idTaskPost)->delete();

          $timestampz = AdmTimeStamp::where('TASKID',$request->idTaskPost)->first();
          // return $timestampz;
          if($timestampz==null){
            $timestamp = new AdmTimeStamp();
          }else{
            $timestamp = AdmTimeStamp::where('TASKID',$request->idTaskPost)->first();
          }
          $timestamp->TIMESTART = $request->timestampValPost;
          $timestamp->TASKID = $cek_task->TASKID;
          $timestamp->MODUI = Session::get('users')->email;
          $timestamp->MODDA = date('Y-m-d H:i:s');
          $timestamp->save();
        }else{
          AdmTimeStamp::where('TASKID', $request->idTaskPost)->delete();

          $rownumz = AdmRowNum::where('TASKID',$request->idTaskPost)->first();
          // return $rownumz;
          if($rownumz==null){
            $rownum = new AdmRowNum();
          }else{
            $rownum = AdmRowNum::where('TASKID',$request->idTaskPost)->first();
          }
          $rownum->ROWNUM = $request->rownumValPost;
          $rownum->ROWNUMDATE = date('Y-m-d');
          $rownum->TASKID = $cek_task->TASKID;
          $rownum->MODUI = Session::get('users')->email;
          $rownum->MODDA = date('Y-m-d H:i:s');
          $rownum->save();
        }

      }else{
        // dd("sini");
        // $rownum = AdmRowNum::where('TASKID',$request->idTaskPost)->first();
        // $rownum->ROWNUM = 0;
        // $rownum->ROWNUMDATE = date('Y-m-d');
        // $rownum->TASKID = $cek_task->TASKID;
        // $rownum->MODUI = Auth::user()->name;
        // $rownum->MODDA = date('Y-m-d H:i:s');
        // $rownum->save();
      }

      $act = "Task Configuration";
      $desc = "Update Task Configuration";
      $activityLog->logUser($act, $desc);

      $data[] = array(
        'result' => 'success'
      );

      return json_encode($data);

    }

    public function deleteTable($id){

      AdmMapTable::where('MAPTBLID', $id)->destroy();

    }

    public function create(activityLog $activityLog){

      $act = "Task Configuration";
      $desc = "Opening form create task configuration";
      $activityLog->logUser($act, $desc);

      $dbSource   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',0)->get();
      $dbDest   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',1)->get();
      $task = null;
        // return view('management.mapping.mapping')
        return view('management.mapping.mappingReboot')
        ->with('dbSource', $dbSource)
        ->with('dbTarget', $dbDest)
        ->with('task', $task);
    }

    public function checkTable($taskId){

      $cek_table = AdmMapTable::where('TASKID', $taskId)->count();

      if($cek_table!=0){
        $cek_table = AdmMapTable::where('TASKID', $taskId)->get();
      }else{
        $cek_table = "empty";
      }

      $data[] = array(
        'data' => $cek_table
      );

      return json_encode($data);
    }
    public function updateTable($jenis, $table, $idtable, $taskId){

      if($idtable=="nan"){

        if($jenis=="target"){

          $exp1 = explode('.', $table);
          // dd($exp1);
          $addTable = new AdmMapTable();
          $addTable->TBLDEST = $exp1[1];
          $addTable->TASKID = $taskId;
          $addTable->INSUI = Session::get('users')->email;
          $addTable->INSDA = date('Y-m-d H:i:s');
          $addTable->save();

        }else if($jenis=="source"){

          $exp1 = explode('.', $table);

          $addTable = new AdmMapTable();
          $addTable->TBLSOURCE = $exp1[1];
          $addTable->TASKID = $taskId;
          $addTable->INSUI = Session::get('users')->email;
          $addTable->INSDA = date('Y-m-d H:i:s');
          $addTable->save();

        }

        $cekTable = AdmMapTable::where('TASKID', $taskId)->orderby('MAPTBLID','desc')->first();
        // dd($cekTable);
        $data[] = array(
          'tableId' => $cekTable->MAPTBLID
        );
        return json_encode($data);
      }else{

        $cekTable = AdmMapTable::where('MAPTBLID', $idtable)->first();

        if($jenis=="target"){

          $exp1 = explode('.', $table);
          // return $exp1[1];
          $cekTable->TBLDEST = $exp1[1];
          $cekTable->TASKID = $taskId;
          $cekTable->MODUI = Session::get('users')->email;
          $cekTable->MODDA = date('Y-m-d H:i:s');
          $cekTable->save();

        }else if($jenis=="source"){

          $exp1 = explode('.', $table);

          $cekTable->TBLSOURCE = $exp1[1];
          $cekTable->TASKID = $taskId;
          $cekTable->MODUI = Session::get('users')->email;
          $cekTable->MODDA = date('Y-m-d H:i:s');
          $cekTable->save();

        }

        $idTableUp = $idtable;

      }

      $data[] = array(
        'tableId' => $idtable
      );
      return json_encode($data);

    }

    public function admTableUpdate(Request $request){

      $cekTable = AdmMapTable::where('MAPTBLID', $request->idTableTxt)->first();
      $cekTable->SCRIPTQUERY = $request->queryText;
      $cekTable->MODUI = Session::get('users')->email;
      $cekTable->MODDA = date('Y-m-d H:i:s');
      $cekTable->save();

    }
    public function admTaskUpdate(Request $request){
      $task = AdmTask::where('TASKID', $request->taskid)->first();
      // dd($task);
      $task->TASKNAME = $request->taskName;
      $task->TASKINTERVAL = $request->taskInterval;
      $task->ISINSERT = $request->proses_type;
      $task->ISROWNUM = $request->data_type;
      $task->PRIMARYKEY1 = $request->primaryKey1;
      $task->PRIMARYKEY2 = $request->primaryKey2;
      $task->DBSOURCEID = $request->selectSource;
      $task->DBDESTID = $request->selectTarget;
      $task->MODUI = Session::get('users')->email;
      $task->MODDA = date('Y-m-d H:i:s');
      $task->save();

      if($request->data_type=="0"){
          $timestamp = AdmTimeStamp::where('TASKID', $request->taskid)->first();
          // dd($timestamp);
          $timestamp->TIMESTART = $request->timestampVal;
          $timestamp->INSDA = date('Y-m-d');
          $timestamp->TASKID = $task->TASKID;
          $timestamp->MODUI = Session::get('users')->email;
          $timestamp->MODDA = date('Y-m-d H:i:s');
          $timestamp->save();
      }else{
        // dd("sini");
        $rownum = AdmRowNum::where('TASKID', $request->taskid)->first();
        $rownum->ROWNUM = $request->rownumVal;
        $rownum->ROWNUMDATE = date('Y-m-d');
        $rownum->TASKID = $task->TASKID;
        $rownum->MODUI = Session::get('users')->email;
        $rownum->MODDA = date('Y-m-d H:i:s');
        $rownum->save();
      }

      return json_encode("done");
    }

    public function edit(activityLog $activityLog, $id){

      $dbSource   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',0)->get();
      $dbDest   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',1)->get();
      $dbTemp   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',0)->get();
      $task = AdmTask::with('dbsource','dbtarget','mapTable','rownum','timestamp')->where('TASKID', $id)->first();
      $field = AdmMapField::where('MAPTBLID', $task->mapTable->MAPTBLID)->get();
      // return $task;

      $getTabSource = $this->getTableSource($task->dbSource->DBCONNID);
      $getTabDest = $this->getTableDestination($task->dbtarget->DBCONNID);
      $getFieldSource = $this->getFieldSource($task->dbSource->DBCONNID,$task->mapTable->TBLSOURCE);
      $getFieldDest = $this->getFieldDestination($task->dbtarget->DBCONNID,$task->mapTable->TBLDEST);

      // $getTabSource = $this->getTableSource($task->dbSource->DBCONNID);
      // $getTabDest = $this->getTableSource($task->dbSource->DBCONNID);
      // $getFieldSource = $this->getFieldSource($task->dbSource->DBCONNID,$task->mapTable->TBLSOURCE);
      // $getFieldDest = $this->getFieldSource($task- >dbSource->DBCONNID,$task->mapTable->TBLSOURCE);
      // dd($getTabDest);
      // return $getFieldSource;
         if($task->ISROWNUM=="1" || $task->ISROWNUM==1){
          $type_data = AdmRowNum::where('TASKID', $task->TASKID)->first();
        }else{
          $type_data = AdmTimeStamp::where('TASKID', $task->TASKID)->first();
        }
        // return $type_data->TIMESTART;

        $act = "Task Configuration";
        $desc = "Opening form edit task configuration";
        $activityLog->logUser($act, $desc);
        // return $dbDest;
        return view('management.mapping.edit')
        ->with('dbSource', $dbSource)
        ->with('dbTarget', $dbDest)
        ->with('dbTemp', $dbTemp)
        ->with('task', $task)
        ->with('type_data', $type_data)
        ->with('tblSource', $getTabSource)
        ->with('tblDest', $getTabDest)
        ->with('getFieldSource', $getFieldSource)
        ->with('getFieldDest', $getFieldDest)
        ->with('field', $field);
    }

    public function getFieldSource($dbconnid,$TABLE_NAME){

      $DBConnection = new DBConnection;
      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();
      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      $columns = array();

      // $TABLE_SCHEMA = explode('.', $tablename)[0];
      // $TABLE_NAME = explode('.', $tablename)[1];

    		$result = $connection->table('information_schema.columns')->where('table_name',$TABLE_NAME)->where('table_schema','public')->orderBy('column_name','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->column_name,
	    			'DATA_TYPE'=>$column->udt_name,
	    		);
	    	}

      $DBConnection->disconnect($AdmDBCon->DBPROVIDER);

      return $columns;

    }
    public function getFieldDestination($dbconnid,$TABLE_NAME){

      $DBConnection = new DBConnection;
      $AdmDBCon   = AdmDBCon::where('DBCONNID',$dbconnid)->first();
      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      $columns    = array();

    		$result = $connection->table('INFORMATION_SCHEMA.COLUMNS')->where('TABLE_NAME',$TABLE_NAME)->where('TABLE_SCHEMA','dbo')->orderBy('COLUMN_NAME','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->COLUMN_NAME,
	    			'DATA_TYPE'=>$column->DATA_TYPE,
	    		);
	    	}

        $DBConnection->disconnect($AdmDBCon->DBPROVIDER);
      	return $columns;

    }
    public function getTableSource($dbconnid){

      $DBConnection = new DBConnection;

      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',0)->first();
      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      // dd($connection);
      $tables = array();

      $result = $connection->table('information_schema.tables')->orderBy('table_schema','asc')->orderBy('table_name','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->table_schema.'.'.$table->table_name;
	    		$tables[] = $table->table_name;
	    	}

    	$DBConnection->disconnect($AdmDBCon->DBPROVIDER);

      return $tables;
    }

    public function getTableDestination($dbconnid){

      $DBConnection = new DBConnection;
      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',1)->first();

      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      $tables = array();

    		$result = $connection->table('INFORMATION_SCHEMA.TABLES')->orderBy('TABLE_SCHEMA','asc')->orderBy('TABLE_NAME','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->TABLE_SCHEMA.'.'.$table->TABLE_NAME;
	    		$tables[] = $table->TABLE_NAME;
	    	}

        $DBConnection->disconnect($AdmDBCon->DBPROVIDER);

        return $tables;

    }

    public function getDatabaseTable(DBConnection $DBConnection, $dbconnid, $type){
      ini_set('memory_limit', '-1');

      if($type=="pgsql"){
        $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',0)->first();
      }else{
        $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',1)->first();
      }

      // dd($AdmDBCon);
      // dd($AdmDBCon);
    	$connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      // return;
    	$tables = array();

    	if($AdmDBCon->DBPROVIDER == 'SQL Server'){
    		$result = $connection->table('INFORMATION_SCHEMA.TABLES')->orderBy('TABLE_SCHEMA','asc')->orderBy('TABLE_NAME','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->TABLE_SCHEMA.'.'.$table->TABLE_NAME;
	    		$tables[] = $table->TABLE_NAME;
	    	}
    	}

    	if($AdmDBCon->DBPROVIDER == 'PostgreSQL'){
    		$result = $connection->table('information_schema.tables')->orderBy('table_schema','asc')->orderBy('table_name','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->table_schema.'.'.$table->table_name;
	    		$tables[] = $table->table_name;
	    	}
	    }

    	$DBConnection->disconnect($AdmDBCon->DBPROVIDER);

    	echo json_encode($tables);
    }

    public function getTableDefinition($dbconnid,$tablename,DBConnection $DBConnection){
    	$AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();

    	$connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);

    	$columns = array();

    	// $TABLE_SCHEMA = explode('.', $tablename)[0];
    	// $TABLE_NAME = explode('.', $tablename)[1];
    	$TABLE_NAME = explode('.', $tablename);

    	if($AdmDBCon->DBPROVIDER == 'SQL Server'){
    		$result = $connection->table('INFORMATION_SCHEMA.COLUMNS')->where('TABLE_NAME',$TABLE_NAME)->where('TABLE_SCHEMA','dbo')->orderBy('COLUMN_NAME','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->COLUMN_NAME,
	    			'DATA_TYPE'=>$column->DATA_TYPE,
	    		);
	    	}
    	}

    	if($AdmDBCon->DBPROVIDER == 'PostgreSQL'){
    		$result = $connection->table('information_schema.columns')->where('table_name',$TABLE_NAME)->where('table_schema','public')->orderBy('column_name','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->column_name,
	    			'DATA_TYPE'=>$column->udt_name,
	    		);
	    	}
	    }

    	$DBConnection->disconnect($AdmDBCon->DBPROVIDER);
      // return count($columns);
    	echo json_encode($columns);
    }

}
