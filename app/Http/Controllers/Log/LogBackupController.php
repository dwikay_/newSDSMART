<?php

namespace App\Http\Controllers\Log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\Log;
use App\Model\AdmRowNum;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use App\Library\activityLog;

class LogBackupController extends Controller
{
  public function indexLogBackup(activityLog $activityLog){

    $task = AdmTask::all();

    $act = "Backup Log Manager";
    $desc = "Opening Backup Log Manager ";
    $activityLog->logUser($act, $desc);

    return view('logManager.backupLog')
    ->with('task', $task);
  }

  public function download(Request $request, activityLog $activityLog, $taskid, $startdate, $enddate){

    if($request->taskId=="all"){
        $log = Log::whereBetween('INSDA', [$request->startDate.' 00:00:00', $request->endDate.' 23:59:59'])->get();
      }else{
        $log = Log::where('TASKID', $request->taskId)
        ->whereBetween('INSDA', [$request->startDate.' 00:00:00', $request->endDate.' 23:59:59'])->get();
      }

    $act = "Backup Log Manager";
    $desc = "Download Backup Log Manager on date ".$startdate." sd ".$enddate;
    $activityLog->logUser($act, $desc);

    $cek_task = AdmTask::where('TASKID', $taskid)->first();
    Excel::create("LOG-DSSMART ".$cek_task->TASKNAME." ".$startdate." ".$enddate, function ($result) use ($log) {
  				$result->sheet('SheetName', function ($sheet) use ($log) {
            $i = 1;
  						foreach ($log as $key => $item) {

                if($item->ISSTAGING=="1" || $item->ISSTAGING==1){
                  $staging = "Staging";
		              $squery = "Insert";
                }else{
                  $staging = "Non Staging";
		              $squery = "Insert & Update";
                }

                if($item->ISMANUALJOB=="1" || $item->ISMANUALJOB==1){
                  $ismanualjob = "Manual Job";
                }else{
                  $ismanualjob = "Normal Job";
                }
  							$data=[];
  							array_push($data, array(
  							$item->TASKNAME,
  							$item->DATEPROC,
  							$item->DBSOURCEHOST,
  							$item->DBDESTHOST,
  							$item->DBSOURCENAME,
  							$item->TBLSOURCE,
  							$item->DBDESTNAME,
  							$item->TBLDEST,
  							$squery ,
                $item->PKSOURCE,
                $item->PKDESTINATION,
                $staging,
                $ismanualjob,
                $item->DATELOG,
                $item->STARTTIMEREFRESH,
                $item->ENDTIMEREFRESH,
                $item->DURATIONTIMEREFRESH,
                $item->STARTTIMEGETDATA,
                $item->FINISHTIMEGETDATA,
                $item->DURATIONTIMEGETDATA,
                $item->STARTTIMEINSERT,
                $item->ENDTIMEINSERT,
                $item->DURATIONTIMEINSERT,
                $item->STARTROWNUM,
                $item->RECORDGETDATA,
                $item->ENDROWNUM,
                $item->RECORDINSERTDATA,
		            $item->TOTALDURATION

  					));
  								$sheet->fromArray($data, null, 'A2', false, false);
  						}
  						$sheet->row(1, array(

                'Task Name',
                'Date Process',
                'IP Database Source',
                'IP Database Destination',
                'Database Source',
                'Table Source',
                'Database Destination',
                'Table Destination',
                'Process Type',
                'PK Source',
                'PK Destination',
                'Flag Staging',
                'Process Job',
                'Date',
                'Start Time Get Data from Staging',
                'Finish Time Get Data from Staging',
                'Duration (s) Time Get Data from Staging', //13
                'Start Time Get Data from View',
                'Finish Time Get Data from View',
                'Duration (s) Time Get Data from View', //16
                'Start Time Insert Data to DWH',
                'Finish Time Insert Data to DWH',
                'Duration (s) Time Insert Data to DWH  (s)', //19
                'Record Start Sequence Rownum from SD-Smarts',
                'Record Get Data from SD-Smarts',
                'Record Finish Sequence Rownum from SD-Smarts',
                'Record Finish insert data to DWH',
		  'Total Duration (s)'
              ));

  						$sheet->setBorder('A1:AA1', 'thin');
  						$sheet->cells('A1:AA1', function ($cells) {
  								$cells->setBackground('#0070c0');
  								$cells->setFontColor('#ffffff');
  								$cells->setValignment('center');
  								$cells->setFontSize('11');
  						});
  						$sheet->setHeight(array(
  					'1' => '20'
  			));
  						$sheet->setWidth('A', '10');
  						$sheet->setWidth('B', '25');
  						$sheet->setWidth('C', '25');
  						$sheet->setWidth('D', '25');
  						$sheet->setWidth('E', '25');
  						$sheet->setWidth('F', '25');
  						$sheet->setWidth('G', '100');
  						$sheet->setWidth('H', '25');
  						$sheet->setWidth('I', '25');
  						$sheet->setWidth('J', '20');
  						$sheet->setWidth('K', '20');
  						$sheet->setWidth('L', '20');
  						$sheet->setWidth('M', '20');
  						$sheet->setWidth('N', '20');
  						$sheet->setWidth('O', '20');
  						$sheet->setWidth('P', '20');
  						$sheet->setWidth('Q', '20');
  						$sheet->setWidth('R', '20');
  						$sheet->setWidth('S', '20');
  						$sheet->setWidth('T', '20');
  						$sheet->setWidth('U', '20');
  						$sheet->setWidth('V', '20');
  						$sheet->setWidth('W', '20');
  						$sheet->setWidth('X', '20');
						$sheet->setWidth('Z', '20');
						$sheet->setWidth('AA', '20');
  				});
  				ob_end_clean();
  		})->download('xls');

  }
}
