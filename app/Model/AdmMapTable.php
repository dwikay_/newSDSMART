<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property int $MAPTBLID
 * @property int $TASKID
 * @property string $TBLSOURCE
 * @property string $TBLDEST
 * @property string $SCRIPTQUERY
 * @property string $INSUI
 * @property string $INSDA
 * @property string $MODUI
 * @property string $MODDA
 */
class AdmMapTable extends Model implements Auditable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    use \OwenIt\Auditing\Auditable;
    protected $table = 'adm_map_table';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'MAPTBLID';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['TASKID', 'TBLSOURCE', 'TBLDEST', 'TBLTEMP', 'SCRIPTQUERY', 'INSUI', 'INSDA', 'MODUI', 'MODDA'];

    // public function task()
    // {
    //     return $this->belongsTo('App\Model\AdmTask', 'TASKID', 'TASKID');
    // }
    //
    // public function mapField(){
    //     return $this->hasMany('App\Model\AdmMapField', 'MAPTBLID', 'MAPTBLID');
    // }

}
