<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class activityUser extends Model
{
    protected $table = 'activitydetail';
    protected $primaryKey = 'id';
    protected $fillable = [
      'username',
      'group',
      'activity',
      'activityDescription',
      'date',
      'time',
    ];

    public function role()
    {
      return $this->hasOne('App\Model\Role', 'id','group');
    }
}
