<?php
namespace App\Library;

use App\Model\activityUser;
use Auth;
use Session;

class activityLog {

  public function logUser($activitys, $desc){

    date_default_timezone_set('asia/jakarta');

    $activity = new activityUser();
    $activity->username = Session::get('email');
    $activity->group = Session::get('role_id');
    $activity->activity = $activitys;
    $activity->activityDescription = $desc;
    $activity->date = date('Y-m-d');
    $activity->time = date('H:i:s');
    $activity->save();
  }

}
?>
