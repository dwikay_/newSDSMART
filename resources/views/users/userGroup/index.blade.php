@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Users Group</h4>
                  <a href="{{url('users/group/create')}}" class="btn btn-sm btn-danger float-right" style="color:white;"><span class="fa fa-plus fa-sm"></span>&nbsp;&nbsp;Add Group</a>
              </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" style="width:100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Role</th>
                        <th>Deskripsi</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

function deletes(id){
  $.confirm({
      title: 'Confirmation',
      content: 'Delete this record ?',
      icon: 'fas fa-exclamation-triangle',
      buttons: {
          confirm: function () {
              location.href="{{url('users/group/delete')}}" + "/" + id;
          },
          cancel: function () {
          }
      }
  });
}

$(document).ready(function() {

var table = $("#lookup").dataTable({
  processing: true,
  serverSide: true,
  ajax:{
    url: "{{ url('users/group/getUsers') }}",
    dataType: "json",
    type: "GET",
    error: function(){  // error handling
      $(".lookup-error").html("");
      $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="4">No data found in the server</th></tr></tbody>');
      $("#lookup_processing").css("display","none");

    }
  },
  columns: [
            {data: 'id'},
            {data: 'role_name'},
            {data: 'description'},
            {data: 'id'}
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).text(row+1);
            },
            orderable: false
          },
          {
            "targets": [3],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).addClass("text-center");
              if(rowData.id==1 || rowData.id=="1"){  
              }else{
                  $(td).append($('@include('inc.button.btnEditDelete')'));
              }
            },
            orderable: false
          },
        ],
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    }
      });

    $('.table').on('click','.btn-edit-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        location.href="{{url('users/group/edit/')}}" + "/" + data[index].id;
    });

    $('.table').on('click','.btn-remove-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        deletes(data[index].id);
    });
});
</script>
@endsection
