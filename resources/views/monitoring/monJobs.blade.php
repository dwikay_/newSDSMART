@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
              </div>
                <h4 class="card-title  btn-job">Monitoring</h4>
                <p class="card-description">Jobs Activities Progress</p>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" style="width:100%">
                    <thead style="">
                      <tr>
                        <th>No.</th>
                        <th>Task Name</th>
                        <th>Process Type</th>
                        <th>Interval (s)</th>
                        <th>DB Source</th>
                        <th>Table Source</th>
                        <th>DB Destination</th>
                        <th>Table Destination</th>
                        <th>Date</th>
                        <th class="text-center">Count Record</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('inc.modals.monitoring')
@endsection
@section('script')
<script>

setInterval(function(){
  $('#lookup').DataTable().ajax.reload(null, false);
}, 15000);

$(document).ready(function() {

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    "searching" : true,
    "scrollX": true,
    'scrollY': '100vh',
    "scrollCollapse": true,
    'autoWidth': true,
    'bSort': true,
    'bPaginate': true,
    ajax:{
      url: "{{ url('monitoring/monJobs/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="12">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'TASKNAME'},
      {data: 'TASKNAME'},
      {data: 'PROSES_TYPE'},
      {data: 'INTERVAL'},
      {data: 'DBSOURCE'},
      {data: 'TBLSOURCE'},
      {data: 'DBDEST'},
      {data: 'TBLDEST'},
      {data: 'DATE'},
      {data: 'COUNTRECORD'},
      {data: 'STATUS'},
      {data: 'TASKID'}
    ],
    columnDefs: [

      {
          "targets": [0],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).text(row+1);
          },
      },
      {
          "targets": [1,2],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).text(cellData);
          },
      },
      {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).text(cellData);
            $(td).addClass("text-center");
          },
      },
      {
          "targets": [4,5,6,7],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).text(cellData);
          },
      },
      {
          "targets": [8],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            // var dates = moment(cellData).locale('en').format('d-MMM-YY');
            // var dates = moment(cellData, ["d-MMM-YY"]);
            $(td).text(cellData);
          },
      },
      {
          "targets": [9],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).text(cellData);
            $(td).addClass("text-center");
          },
      },
      {
          "targets": [10],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            var status;
            var classnya;
            switch(cellData){
              case 'Pending' : status = 'Pending'; classnya = "danger"; break;
              case 'On Progres' : status = 'On Progres'; classnya = "info text-white"; break;
              case 'Complete' : status = 'Complete'; classnya = "success"; break;
              case 'Stop' : status = 'Stop'; classnya = "danger"; break;
              case 'Exception' : status = 'Exception'; classnya = "danger"; break;
            }
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('badge badge-'+classnya)
                        .text(status)
                    )
          },
      },
      {
          "targets": [11],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td)
            .append($('@include('inc.button.btnView')'));
            $(td).addClass('text-center');
          },
      },
    ],
    drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }

  });

  $('.table').on('click','.btn-view', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      $.ajax({
        url: "{!! url('monitoring/monJobs/getOneRecord') !!}/" + data[index].TASKID,
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
        {
          // $('#idrActual').html(data[0]["actualPrice"]);
          loadData(data[0]);
          // console.log(data[0]);
        }
        });

        $('#modal-monitoring').modal();

      // loadData(data[index]);
  });




});
</script>
@endsection
