@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">DB Config</h4>
              <p class="card-description">
                Database Configuration
              </p><br>
              <form id="form" class="forms-sample" action="" method="POST">
                {{csrf_field()}}
                <div class="row ">

                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Server Name</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" placeholder="" value="{{$dbcon->DBSERVERNAME}}" id="systemName" name="systemName">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Provider Name</label>
                      <div class="col-sm-4">
                      <select class="form-control form-control-sm" id="providerName" name="providerName">
                        <option value="MySQL" {{ $dbcon->DBPROVIDER == "MySQL" ? 'selected' : ''}}>Mysql Driver</option>
                        <option value="PostgreSQL" {{ $dbcon->DBPROVIDER =="PostgreSQL" ? 'selected' : ''}}>Postgres SQL</option>
                        <option value="SQL Server"  {{$dbcon->DBPROVIDER =="SQL Server" ? 'selected' : ''}}>SQL Server</option>
                      </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Database Type</label>
                      <div class="col-sm-4">
                      <select class="form-control form-control-sm" id="jenisDB" required name="jenisDB">
                        <option disabled selected></option>
                        <option value="0" {{ $dbcon->TYPEDB == "0" ? 'selected' : ''}}>Source Database</option>
                        <option value="1" {{ $dbcon->TYPEDB == "1" ? 'selected' : ''}}>Destination Database</option>
                      </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Database Name</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" placeholder="" value="{{$dbcon->DBNAME}}" id="databaseName" name="databaseName">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">IP Address</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" placeholder="" value="{{$dbcon->DBHOSTNAME}}" id="ipAddress" name="ipAddress">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">IP Port</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" placeholder="" value="{{$dbcon->DBPORT}}" id="port" name="port">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">User ID</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" placeholder="" value="{{$dbcon->DBUSERID}}" id="userID" name="userID">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Password</label>
                      <div class="col-sm-4">
                        <input type="password" class="form-control form-control-sm" placeholder="" value="{{$dbcon->DBPASSWORD}}" id="password" name="password">
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="col-md-12 mt-20 text-right" style="margin-top:5%">
                    <a class="btn btn-sm btn-info btn-testCon float-left" id="btnCon" style="color:white"><i class="fas fa-network-wired"></i>&nbsp;&nbsp;Test Connection</a>
                    <a class="btn btn-sm btn-danger btn-save" id="btnsave" style="color:white"><i class="fa fa-floppy-o fa-sm"></i>&nbsp;&nbsp;Save Config</a>
                    <a href="{{url('management/config')}}" class="btn btn-sm btn-primary btn-cancel" style="color:white"><i class="fa fa-arrow-left fa-sm"></i>&nbsp;Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

  $('#btnsave').on('click', function(){

      $('#form').attr('action','{{url('management/config/update/'.$dbcon->DBCONNID)}}');
      $('form').submit();
  });

  $('#btnCon').on('click', function(){
    submitForm();
  });

  function submitForm(){
      var str = $('form').serialize();
      console.log(str);
      $.ajax({
        url: "{!! url('management/config/testConnection') !!}?" + str,
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
          {
            $.confirm({
                title: 'Information',
                content: data,
                buttons: {
                    ok: function () {
                    },
                }
            });

            // console.log();
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              console.log(errorMsg);
          }
        });
  }

</script>
@endsection
